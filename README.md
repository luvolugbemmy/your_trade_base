# YourTradeBase

## Problem Statement

>We have some customer data in a text file (people.json, attached) JSON-encoded. We want to find all people living within 100km of Bristol (GPS coordinates 51.450167, -2.594678) who live in England.

>Firstly write a program that will read the full list of people and output (in JSON format) the id, full name, value, and email of matching customers (within 100km), sorted by value (descending).

>Secondly we want to find the average customer value of all people living within 200km of the same location (GPS coordinates 51.450167, -2.594678), write a program that outputs the value for this.

>The task should take between 1-3 hours to complete and can be written in any language you are most comfortable with (preferably Ruby, for this role). You may use any external libraries or frameworks you feel appropriate and should be written as production quality code.

>Your program should use the haversine formula to calculate distances, include a full test suite and be submitted as a git repository. You should also include instructions on how to run the application.

## Solution

I wrote a program (gem) that reads and parses the people json file and a method to solve each of the problem statment above.

Based on the requirement to calculate distance using `haversine formula` I use a gem called `haversine_distance_c`.
I also created an enumerable method `.within` that returns people within the distance of a given coordinate


## How to run  program

While in the root folder of the project run:

```ruby
bundle install
```

```ruby
ruby lib/your_trade_base.rb
```

## How to run program tests

While in the root folder of the project run:

```ruby
 bundle exe rspec
```

## References
 - https://github.com/deliveroo/haversine_distance


## Contributing

Bug reports and pull requests are welcome on GitHub at https://bitbucket.org/luvolugbemmy/your_trade_base. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [Contributor Covenant](http://contributor-covenant.org) code of conduct.

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).

## Code of Conduct

Everyone interacting in the YourTradeBase project’s codebases, issue trackers, chat rooms and mailing lists is expected to follow the [code of conduct](https://bitbucket.org/luvolugbemmy/your_trade_base/src/master/CODE_OF_CONDUCT.md).
