# frozen_string_literal: true

lib = File.expand_path(__dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)

require 'your_trade_base/version'
require 'your_trade_base/people'
require 'json'
require 'enumerable'

# lib/your_trade_base
module YourTradeBase
  class << self
    include Enumerable
    attr_reader :people

    def run
      @people = YourTradeBase::People.all
      coordinates = [51.450167, -2.594678]
      puts '***Output (in JSON format) the id, full name, value, and email' \
           'of matching customers (within 100km), of Bristol (GPS coordinates '\
           " 51.450167, -2.594678) sorted by value (descending).: \n"
      puts people_within(coordinates, 100)
      puts "\n\n\n"

      puts '***The average customer value of all people living within '\
           "200km of GPS Coordinate(#{coordinates.join(', ')}) is:  \n"
      puts avg_value_for_people_within(coordinates, 200)
      puts "\n\n\n"
    end

    def people_within(coordinates, distance)
      result = people.within(coordinates, distance)
      result = result.map do |r|
        name = r['name']
        r['full_name'] = [name['first'], name['last']].compact.join(' ')
        r.select { |key, _| %w[id full_name value email].include? key }
      end
      result.sort_by { |r| -r['value'].to_f }.to_json
    end

    def avg_value_for_people_within(coordinates, distance)
      result = people.within(coordinates, distance)
      values = result.collect { |r| r['value'] }.compact.map(&:to_f)
      values.sum / result.count
    end

    def root_path
      File.expand_path('../', __dir__)
    end
  end

  class Error < StandardError; end
end

YourTradeBase.run
