# frozen_string_literal: true

require 'haversine_distance'

# Distance Enumerable module
module Enumerable
  def within(coordinates, distance)
    result = []
    each do |e|
      e_loc = e['location']
      next if e_loc.empty?

      km = HaversineDistance.km(e_loc['latitude'].to_f, e_loc['longitude'].to_f,
                                coordinates.first, coordinates.last)
      result << e if km <= distance
    end
    result
  end
end
