# frozen_string_literal: true

module YourTradeBase
  # People Class
  class People
    def self.all
      JSON.parse(
        File.read(File.join(YourTradeBase.root_path, 'storage', 'people.json'))
      )
    end
  end
end
