# frozen_string_literal: true

RSpec.describe YourTradeBase::People do
  it 'reads the full list of people from the people.json' do
    expect(described_class.all.count).to eq 100
  end
end
