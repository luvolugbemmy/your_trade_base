# frozen_string_literal: true

RSpec.describe YourTradeBase do
  let(:coordinates) { [51.450167, -2.594678] }

  it 'has a version number' do
    expect(YourTradeBase::VERSION).not_to be nil
  end

  context 'when finding people' do
    context 'with dist. within 100km of coordinates 51.450167, -2.594678' do
      let(:expected_result) do
        File.read(
          File.join("#{described_class.root_path}/spec/fixtures", 'files',
                    'people_within_100km.json')
        )
      end

      it 'returns the people JSON result for the given criteria' do
        expect(
          described_class.people_within(coordinates, 100)
        ).to eq(expected_result)
        expect(
          described_class.people_within(coordinates, 200)
        ).not_to eq(expected_result)
      end
    end
  end

  context 'when finding average customer value of all people' do
    context 'with dist. within 200km of coordinates 51.450167, -2.594678' do
      let(:expected_result) { 2467.727586206897 }
      it 'calculates the people avg values for the given criteria' do
        expect(
          described_class.avg_value_for_people_within(coordinates, 200)
        ).to eq(expected_result)
        expect(
          described_class.avg_value_for_people_within(coordinates, 100)
        ).not_to eq(expected_result)
      end
    end
  end
end
